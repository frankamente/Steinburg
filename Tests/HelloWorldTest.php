<?php
class HelloWorldTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PDO
     */
    private $pdo;
    public function setUp()
    {
        
    }
    public function tearDown()
    {
        
    }
    public function testHelloWorld()
    {
        $this->assertEquals('Hello World', 'Hello World');
    }
    public function testHello()
    {
        $this->assertEquals('Hello Bar', 'Hello Bar');
    }
    public function testWhat()
    {
       
        $this->assertEquals('Bar', 'Bar');
    }
}