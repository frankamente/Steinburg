<?php

class View {

    public static function loadTemplate($template, $data) {
        ob_start();
        require 'view/templates/'.$template.'.php';
        $contenido = ob_get_clean();
        include 'view/layout.php';
    }

}
