<?php

class LoginService {

    public function __construct() {
        include 'model/ModelLogin.php';
    }

    public function loginAction() {
        $modelLogin = new ModelLogin();
        $modelLogin->setUsername($_POST['username']);
        $modelLogin->setPassword($_POST['password']);
        $result = $modelLogin->loginAction();
        if (mysqli_num_rows($result) == 1) {
            $fila = mysqli_fetch_array($result);
            session_start();
            $_SESSION['username'] = $fila['username'];
            $_SESSION['loggedIn'] = true;
            return true;
        } else {
            return false;
        }
    }

}
