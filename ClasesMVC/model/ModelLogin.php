<?php

class ModelLogin {

    private $username;
    private $password;
    private $id;
    private $correo;
    private $connectionDB;

    public function __construct() {
        include 'core/ConnectDB.php';
        $connectionDB = new ConnectDB();
        $this->connectionDB = $connectionDB->connectionDB();
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getId() {
        return $this->id;
    }

    function getCorreo() {
        return $this->correo;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    public function loginAction() {

        $query = "SELECT * from usuarios where username='$this->username' and password='$this->password'";
        return mysqli_query($this->connectionDB, $query);
    }

}
