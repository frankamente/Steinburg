<?php
/* Incluyendo archivos configuración */

include 'core/Core.php';
include 'core/View.php';
if (empty($_GET['controller'])) {
    $controller = "login";
} else {
    $controller = $_GET['controller'];
}

if (empty($_GET['action'])) {
    $action = "index";
} else {
    $action = $_GET['action'];
}
$controller = ucwords($controller) . "Controller";
include 'controller/'.$controller.'.php';
if (class_exists($controller)) {
    $newController = new $controller();
    if(method_exists($newController, $action)){
        $newController->$action();
    }
}
?>
