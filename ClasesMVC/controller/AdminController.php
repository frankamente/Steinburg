<?php

class AdminController {
    
    public function index(){
        session_start();
        if($_SESSION['loggedIn']){
            View::loadTemplate('adminIndex', $_SESSION['username']);
        }
    }
}
