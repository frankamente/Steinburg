<?php

class LoginController{
    
    public function __construct() {
        include 'service/LoginService.php';
    }
    
    public function loginAction(){
        $loginService = new LoginService();
        if($loginService->loginAction()){
            header("location:index.php?controller=Admin&action=index");
        }
    }
    
    public function index(){
        View::loadTemplate('login');
    }
}
